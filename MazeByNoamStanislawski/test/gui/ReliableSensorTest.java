package gui;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import generation.CardinalDirection;
import generation.Floorplan;
import generation.MazeContainer;
import generation.MazeFactory;
import generation.StubOrder;
import gui.Robot.Direction;
import gui.Robot.Turn;
import generation.Order.Builder;

class ReliableSensorTest {

	MazeContainer maze = new MazeContainer();
	StubOrder order = new StubOrder();
	MazeFactory factory = new MazeFactory();
	Controller controller = new Controller();
	Floorplan floorplan;
	
	ReliableRobot robot;
	ReliableSensor s1;
	

	@BeforeEach
	void setUp() throws Exception {
		order.setBuilder(Builder.DFS);
		order.setSkillLevel(1);
		order.setSeed(13);
		order.deliver(maze);
		
		factory.order(order);
		factory.waitTillDelivered();
		
		maze = order.getMaze();
		robot = new ReliableRobot();
       
        robot.setController(controller);
       
        controller.setRobotAndDriver(robot, null);
        controller.turnOffGraphics();
        controller.driverChoice = "";
		controller.switchFromGeneratingToPlaying(maze);
		
		s1 = new ReliableSensor();
		
	}
	@Test
	void ReliableSensorTestCanFunction() throws Exception {
		float[] batarray = new float[] {robot.getBatteryLevel()};
    	
		s1.setMaze(maze);
    	s1.setRobot(robot);
    	s1.setSensorDirection(Direction.FORWARD);
    	robot.rotate(Turn.AROUND);
    	robot.move(1);
    	assert(s1.distanceToObstacle(robot.getCurrentPosition(), robot.getCurrentDirection(), batarray) == 0);
    	robot.rotate(Turn.RIGHT);
    	assert(s1.distanceToObstacle(robot.getCurrentPosition(), robot.getCurrentDirection(), batarray) == 1);
    	robot.move(1);
    	robot.rotate(Turn.RIGHT);
    	try {
			assert(s1.distanceToObstacle(robot.getCurrentPosition(), robot.getCurrentDirection(), batarray) == 4);
		} catch (Exception e) {
			System.out.println("BROKEN TEST");
		}
		
	}

}
