package gui;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import generation.Floorplan;
import generation.MazeContainer;
import generation.MazeFactory;
import generation.StubOrder;
import generation.Order.Builder;
import gui.Robot.Direction;
import gui.Robot.Turn;

class WallFollowerTest {
	MazeContainer maze = new MazeContainer();
	StubOrder order = new StubOrder();
	MazeFactory factory = new MazeFactory();
	Controller controller = new Controller();
	Floorplan floorplan;
	
	ReliableRobot robot;
	WallFollower wallfollower;
	
	@BeforeEach
	void setUp() throws Exception {
		order.setBuilder(Builder.DFS);
		order.setSkillLevel(0);
		order.setSeed(13);
		order.deliver(maze);
		
		factory.order(order);
		factory.waitTillDelivered();
		
		maze = order.getMaze();
		robot = new ReliableRobot();
        wallfollower = new WallFollower();
       
        robot.setController(controller);
        wallfollower.setMaze(maze);
        wallfollower.setRobot(robot);
       
        controller.setRobotAndDriver(robot, wallfollower);
        controller.turnOffGraphics();
		
	}
	/**
	 * Test to see if the wallfollower finds the end position of the maze.
	 * Essentially, does the wallfollower solve the maze.
	 * @throws Exception if the robot is not in the maze.
	 */
	@Test
	void testWallFollowerSolves() throws Exception {
		controller.switchFromGeneratingToPlaying(maze); // this will call drive2Exit
		int[] currentPosition = robot.getCurrentPosition();
		assert(floorplan.isExitPosition(currentPosition[0], currentPosition[1]));
	}
	
	/**
	 * This test sees if the wallfollower truly follows the wall.
	 * It checks if there is a left wall, and if so the distance needs to be 0.
	 * 
	 * @throws Exception if robot out of bounds.
	 */
	@Test
	void testWallFollowerStaysOnWall() throws Exception {
		int[] currentPosition = robot.getCurrentPosition();
		while(!floorplan.isExitPosition(currentPosition[0], currentPosition[1])) {
			wallfollower.drive1Step2Exit();
			if(robot.distanceToObstacle(Direction.LEFT) > 0) {
				robot.rotate(Turn.LEFT);
				assert(robot.distanceToObstacle(Direction.LEFT) == 0);
			}
			else {
				assert(robot.distanceToObstacle(Direction.LEFT) == 0);
			}
		}
	}

}
