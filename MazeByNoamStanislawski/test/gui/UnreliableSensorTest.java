package gui;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import generation.CardinalDirection;
import generation.Floorplan;
import generation.MazeContainer;
import generation.MazeFactory;
import generation.StubOrder;
import gui.Robot.Direction;
import gui.Robot.Turn;
import generation.Order.Builder;

class UnreliableSensorTest {

	MazeContainer maze = new MazeContainer();
	StubOrder order = new StubOrder();
	MazeFactory factory = new MazeFactory();
	Controller controller = new Controller();
	Floorplan floorplan;
	
	UnreliableRobot robot;
	UnreliableSensor s1;
	

	@BeforeEach
	void setUp() throws Exception {
		order.setBuilder(Builder.DFS);
		order.setSkillLevel(1);
		order.setSeed(13);
		order.deliver(maze);
		
		factory.order(order);
		factory.waitTillDelivered();
		
		maze = order.getMaze();
		robot = new UnreliableRobot();
       
        robot.setController(controller);
       
        controller.setRobotAndDriver(robot, null);
        controller.turnOffGraphics();
        controller.driverChoice = "";
        controller.UnreliableSensorsArray = new int[] {1,1,1,1};
		controller.switchFromGeneratingToPlaying(maze);
		
		
	}
	/**
	 * This test shows that the sensor works as a normal sensor when functional.
	 * The expected outputs are based on the standard seed 13 test and the walls as follows.
	 * @throws Exception
	 */
	@Test
	void UnreliableSensorTestCanFunction() throws Exception {
		int[] currentPosition = robot.getCurrentPosition();
		System.out.println(Arrays.toString(currentPosition));
		UnreliableSensor s1 = (UnreliableSensor) robot.forwardSensor;
    	
		float[] batteryArray = new float[] {robot.getBatteryLevel()};

		assert(s1.distanceToObstacle(currentPosition, robot.getCurrentDirection(), batteryArray) == 1);
		robot.rotate(Turn.LEFT);
		assert(s1.distanceToObstacle(currentPosition, robot.getCurrentDirection(), batteryArray) == 0);
		
	}
	/**
	 * This test would see how long it takes for the repair process to activate,
	 * and measure how long it is. It should happen after the thread has been alive for
	 * 4 seconds, and the repair process in total should take 2 seconds total.
	 */
	@Test 
	void UnreliableSensorThreadRepair() {
		
	}
	

}
