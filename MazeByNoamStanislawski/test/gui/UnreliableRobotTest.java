package gui;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import generation.CardinalDirection;
import generation.Floorplan;
import generation.MazeContainer;
import generation.MazeFactory;
import generation.StubOrder;
import generation.Order.Builder;
import gui.Robot.Turn;

class UnreliableRobotTest {

	MazeContainer maze = new MazeContainer();
	StubOrder order = new StubOrder();
	MazeFactory factory = new MazeFactory();
	Controller controller = new Controller();
	Floorplan floorplan;
	
	UnreliableRobot robot;
	
	@BeforeEach
	void setUp() throws Exception {
		order.setBuilder(Builder.DFS);
		order.setSkillLevel(1);
		order.setSeed(13);
		order.deliver(maze);
		
		factory.order(order);
		factory.waitTillDelivered();
		
		maze = order.getMaze();
		robot = new UnreliableRobot();
       
        robot.setController(controller);
       
        controller.setRobotAndDriver(robot, null);
        controller.turnOffGraphics();
        controller.driverChoice = "";
        controller.UnreliableSensorsArray = new int[] {1,1,1,1};
		controller.switchFromGeneratingToPlaying(maze);
	}
	
	/**
	 * This test sees if the robot functions similarly to its Reliable partner.
	 * It will see if the robot can move and rotate normally, as it should be able to do so.
	 */
	@Test
	void UnreliableRobotTestFunctioning() {
		int[] oldPosition = new int[2];
		CardinalDirection oldDirection = robot.getCurrentDirection();
		try {
			oldPosition = robot.getCurrentPosition();
		} catch (Exception e) {}
		
		robot.move(1);
		robot.rotate(Turn.LEFT);
		try {
			assert(!Arrays.equals(oldPosition, robot.getCurrentPosition()));
		} catch (Exception e) {}
		assert(!robot.getCurrentDirection().equals(oldDirection));
	

	}
	
	/**
	 *  This test sees if the Unreliablesensors are truly present and functional.
	 */
	@Test
	void UnreliableRobotTestVerifySensors() {
		assert(robot.backSensor instanceof UnreliableSensor);
		assert(robot.forwardSensor instanceof UnreliableSensor);
		assert(robot.leftSensor instanceof UnreliableSensor);
		assert(robot.rightSensor instanceof UnreliableSensor);
	}
	
	/**
	 * Checks to see if the threads of the sensors are existent.
	 * They should all exist, as the command line arg would be 1111.
	 */
	@Test
	void UnreliableRobotTestSensorThreads() {
		
		
	}
	
	/**
	 * If a sensor is not failed, it should be able to give the distance.
	 * First checks which sensors are awake, and then tries to call their distanceToObstacleMethod.
	 * @throws Exception 
	 * 
	 */
	@Test
	void UnreliableRobotTestVerifySensorFunctionality() throws Exception {
		float[] powersupply = new float[] {robot.getBatteryLevel()};
		if(!controller.forwardThread.getState().equals(Thread.State.TIMED_WAITING)) {
			int forwardLength = robot.forwardSensor.distanceToObstacle(robot.getCurrentPosition(), robot.getCurrentDirection(), powersupply);
			assert(forwardLength == 1);
		}
		if(!controller.backThread.getState().equals(Thread.State.TIMED_WAITING)) {
			
		}
		if(!controller.leftThread.getState().equals(Thread.State.TIMED_WAITING)) {
			
		}
		if(!controller.rightThread.getState().equals(Thread.State.TIMED_WAITING)) {
			
		}
	}
}
