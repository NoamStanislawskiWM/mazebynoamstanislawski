package gui;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import generation.Floorplan;
import generation.MazeContainer;
import generation.MazeFactory;
import generation.StubOrder;
import generation.Order.Builder;

class WizardTest {

	MazeContainer maze = new MazeContainer();
	StubOrder order = new StubOrder();
	MazeFactory factory = new MazeFactory();
	Controller controller = new Controller();
	Floorplan floorplan;
	
	ReliableRobot robot;
	Wizard wizard;
	
	@BeforeEach
	void setUp() throws Exception {
		order.setBuilder(Builder.DFS);
		order.setSkillLevel(0);
		order.setSeed(13);
		order.deliver(maze);
		
		factory.order(order);
		factory.waitTillDelivered();
		
		maze = order.getMaze();
		robot = new ReliableRobot();
        wizard = new Wizard();
       
        robot.setController(controller);
        wizard.setMaze(maze);
        wizard.setRobot(robot);
       
        controller.setRobotAndDriver(robot, wizard);
        controller.turnOffGraphics();
		controller.switchFromGeneratingToPlaying(maze);
	}

	@Test
	void test() {
		fail("Not yet implemented");
	}

}
