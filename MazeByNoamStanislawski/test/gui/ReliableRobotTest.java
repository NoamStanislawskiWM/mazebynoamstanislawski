package gui;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import generation.CardinalDirection;
import generation.MazeContainer;
import generation.MazeFactory;
import generation.Order.Builder;
import generation.StubOrder;
import generation.Floorplan;
import gui.Constants.UserInput;
import gui.Robot.Direction;
import gui.Robot.Turn;

/**
 * 
 * @author Noam Stanislawski
 *
 */
class ReliableRobotTest {
	
	
	MazeContainer maze = new MazeContainer();
	StubOrder order = new StubOrder();
	MazeFactory factory = new MazeFactory();
	Controller controller = new Controller();
	Floorplan floorplan;
	
	ReliableRobot robot;
	Wizard wizard;
	

	@BeforeEach
	void setUp() throws Exception {
		order.setBuilder(Builder.DFS);
		order.setSkillLevel(0);
		order.setSeed(13);
		order.deliver(maze);
		
		factory.order(order);
		factory.waitTillDelivered();
		
		maze = order.getMaze();
		robot = new ReliableRobot();
        wizard = new Wizard();
       
        robot.setController(controller);
        wizard.setMaze(maze);
        wizard.setRobot(robot);
       
        controller.setRobotAndDriver(robot, wizard);
        controller.turnOffGraphics();
		controller.switchFromGeneratingToPlaying(maze);
		
	}

	@Test
	void testReliableRobotInitialPosition() throws Exception {
		assert(robot.getCurrentPosition() instanceof int[]);
	}

	@Test
	void testReliableRobotInitialGetDirection() {
		assert(robot.getCurrentDirection() instanceof CardinalDirection);
	}
	
	@Test
	void testReliableRobotBatteryLevel() {
		assert(robot.getBatteryLevel() == 3500);
		robot.setBatteryLevel(2000);
		assert(robot.getBatteryLevel() == 2000);
		robot.rotate(Turn.LEFT);
		assert(robot.getBatteryLevel() == 1997);
		robot.rotate(Turn.AROUND);
		assert(robot.getBatteryLevel() == 1991);
	}
	
	@Test
	void testReliableRobotRotate() {
		assert(robot.getCurrentDirection() instanceof CardinalDirection);
		CardinalDirection oldDirection = robot.getCurrentDirection();
		robot.rotate(Turn.LEFT);
		CardinalDirection newDirection = robot.getCurrentDirection();
		robot.rotate(Turn.RIGHT);
		CardinalDirection thirdDirection = robot.getCurrentDirection();
		assert(!oldDirection.equals(newDirection));
		assert(oldDirection.equals(thirdDirection));
	}
	
	@Test
	void testReliableRobotMove() throws Exception {
		int[] oldLoc = robot.getCurrentPosition(); 
		robot.rotate(Turn.RIGHT);
		robot.move(1);
		int[] newLoc = robot.getCurrentPosition();
		robot.rotate(Turn.AROUND);
		robot.move(1);
		int[] thirdLoc = robot.getCurrentPosition();
		assert(!Arrays.equals(oldLoc, newLoc));
		assert(Arrays.equals(oldLoc, thirdLoc));
	}
	
	@Test
	void testReliableRobotIsAtExit() {
		boolean isAtExit = robot.isAtExit();
		assert(isAtExit == false);
	}
	
	@Test 
	void testReliableRobotIsInRoom() {
		boolean isInRoom = robot.isInsideRoom();
		assert(isInRoom == false);
	}
	
	@Test
	void testReliableRobotJump() {
		
	}
	
	@Test 
	void testReliableRobotDistanceToObstacle() {
		robot.rotate(Turn.RIGHT);
    	//robot.move(1);
    	robot.rotate(Turn.LEFT);
    	int distanceToWall = robot.distanceToObstacle(Direction.FORWARD);
    	try {
			System.out.println(Arrays.toString(robot.getCurrentPosition()));
		} catch (Exception e) {
		}
    	System.out.println(distanceToWall);
	}
}
