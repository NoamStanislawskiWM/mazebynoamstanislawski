package generation;


public class StubOrder implements Order {
	private int level;
	Builder builder;
	boolean isperfect;
	int seed;
	MazeContainer maze;
	
	@Override
	public int getSkillLevel() {
		return level;
	}

	public void setSkillLevel(int newLevel) {
		level = newLevel;
	}

	@Override
	public Builder getBuilder() {
		return builder;
	}
	
	public void setBuilder(Builder newBuilder) {
		builder = newBuilder;
	}
	
	

	@Override
	public boolean isPerfect() {
		return isperfect;
	}

	@Override
	public int getSeed() {
		return seed;
	}
	
	public void setSeed(int newSeed) {
		seed = newSeed;
	}

	@Override
	public void deliver(Maze mazeConfig) {
		maze = (MazeContainer) mazeConfig;
	}
	
	public MazeContainer getMaze() {
		return maze;
	}

	@Override
	public void updateProgress(int percentage) {
	}

}
