package generation;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import generation.Order.Builder;


class MazeFactoryTest {
	/**
	 * For some reason I can not get the methods from maze or floorplan to be recognized by a JUnit Test.
	 * My guess is that I have instantiated them incorrectly? FLoorplan test seems to work fine but I cannot 
	 * replicate said success.
	 * 
	 */
	
	private MazeFactory factory = new MazeFactory();
	private StubOrder order = new StubOrder();
	private MazeContainer maze;
	private Distance distance;
	private Floorplan floorplan;
	private int level;
	
	/**
	 * Set up doing the testing
	 * 
	 * 
	 */
	private final void setUp() {

		order.setBuilder(Builder.DFS);
		factory.order(order);
		factory.waitTillDelivered();
		level = order.getSkillLevel();
		maze = order.getMaze();
		floorplan = maze.getFloorplan();

	}
	/**
	 * Iterate through each cell of the maze and check for border walls
	 * Need to ensure that there are no enclosed spaces (4 border walls)
	 * If every spot has at least one opening, then should be solvable and a true maze.
	 */
	@Test
	public final void testMazeFactoryValidMaze() {
		
		for(int i = 0; i < maze.getWidth(); i++) {
			for(int j = 0; j < maze.getHeight(); j++) {
				assert(maze.isValidPosition(i, j));
				assert(maze.getDistanceToExit(i, j) < maze.getHeight() * maze.getWidth());
			}
		}
	}
	
	/** Check where the starting position is, and it's not in the exit position
	 * Compare the two x,y coordinates of the two spots
	 */	
	@Test
	public final void testMazeFactoryStartingPosition() {

		int startX = maze.getStartingPosition()[0];
		int startY = maze.getStartingPosition()[1];
		int distance = maze.getDistanceToExit(startX, startY);
		
		assert(0 < startX && startX < maze.getWidth());
		assert(0 < startY && startY < maze.getHeight());
		assert(distance <=  maze.getWidth() *  maze.getHeight());
		
	}
	
	/** 
	 * Test where the exit is, and it's distance to the start
	 * Make sure that is the max distance away from starting position
	 * If a value in distance matrix is greater than starting position distance, fail.
	 */
	@Test
	public final void testMazeFactoryExitPositionMax() {

	}

	
	/**
	 * Check the order to see if the maze should be perfect
	 * Check the maze to see if there are any rooms
	 * If there is a single room, fail. Otherwise, pass.
	 */
	@Test
	public final void testMazeFactoryPerfect() {

	}
	
	/**
	 * Check the dimensions as determined by the skill level.
	 * If dimensions in maze correspond to dimensions in constants array, pass.
	 * Otherwise fail.
	 */
	@Test
	public final void testMazeFactoryDimensions() {

	}
	
	/**
	 * Compare the builder within the factory to the builder in maze (really maze container).
	 * If they are the same (from the enum of the builders), then 
	 */
	@Test
	public final void testMazeFactoryBuilderType() {

	}

	/**
	 * Test whether the seed as given in the order has been used for the maze generation.
	 * If the integers gathered from both are the same, pass.
	 * Also checks that they are valid seeds (of type int). 
	 */
	public final void testMazeFactorySeed() {

	}

}
