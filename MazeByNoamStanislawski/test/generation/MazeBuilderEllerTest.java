package generation;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import generation.Order.Builder;

class MazeBuilderEllerTest {

	MazeFactory factory = new MazeFactory();
	StubOrder order = new StubOrder();
	
	MazeContainer maze;
	Distance distance;
	MazeBuilder builder;
	Floorplan floorplan;
	int level = 0;
	
	
	@BeforeEach
	void setUp() throws Exception {
		
		order.setBuilder(Builder.Eller);
		order.setSkillLevel(level);
		factory.order(order);
		factory.waitTillDelivered();
		maze = order.getMaze();
		floorplan = maze.getFloorplan();
		
	}
	
	
	@Test
	public final void testEllerFirstRow() {
		/*
		 * Test if first row is correctly set up, with some walls broken
		 * 
		 */
	}
//
//	@Test
//	public final void testEllerSingleSet() {
//		/*
//		 * Test if there is a single set compromsinging all cells.
//		 * 
//		 */
//	}
//	
//	@Test
//	public final void testEllerSolutions() {
//		/*
//		 * Test there is only one solution (no loops) 
//		 * 
//		 */
//	}

}
