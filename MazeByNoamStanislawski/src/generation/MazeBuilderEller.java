package generation;

import gui.Constants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

class MazeBuilderEller extends MazeBuilder implements Runnable {
	
	/*
	 * I know this is extremely buggy, but I have tried my hardest with the time I have used.
	 * Sometimes, it works fine.
	 * Usually, it doesnt.
	 * 
	 * Main issues:
	 * 1) Spawn in a 1x1 enclosed sometimes.
	 * 2) Spawn right at the exit
	 * 3) Enclosed spaces exist
	 * 4) Rooms exist sometimes when they shouldn't? 
	 * 
	 * In order to fix the last two I kept trying to add new qualifications for adding vertical connections, as 
	 * that was my guess as to where the problem is.
	 * 
	 * The one issue I really do not understand is 2), as I have no clue why it spawns there. The exit and entrance should be 
	 * max distance away, which makes sense to spawn in an enclosed 1x1 (distance is infinity), but spawning right next door
	 * does not make sense to me. Nonetheless, it is what it is.
	 */
	
	
	protected ArrayList<int[]> cellsets;
	Random random = new Random();

	public MazeBuilderEller() {
		super();
		ArrayList<int[]> cellsets = new ArrayList<int[]>(); // will store different sets of cells
		this.cellsets = cellsets;
		System.out.println("MazeBuilderEller uses Eller's algorithm to generate maze.");

		}

	
	
	@Override
	/**
	 * Generate Pathways is the method to override for running Eller's Algorithm to generate said Maze
	 * Starts off with a full floorplan, all walls up, as dicated by MazeBuilder's run() method. 
	 */
	protected void generatePathways() {
		int setnumber = 0; // used to assign set numbers
		for(int j = 0; j < height; j++) { 
			for(int i = 0; i < width; i++) {
				cellsets.add(new int[] {i, j, setnumber});
				setnumber++;
			}
		}
		
		generateEller();
		CardinalDirection[] directions = CardinalDirection.values();
		
		int[] startingpos = new int[] {startx,starty} ;
		for(int i = 0; i < cellsets.size(); i++) {
			if(startingpos[0] == cellsets.get(i)[0] && cellsets.get(i)[2] == startingpos[1]) {
				CardinalDirection walldirect = directions[random.nextInt(4)];
				
			}
		}
	}
	
	/**
	 * This is the main Eller's algorithm method.
	 * It generates top to bottom, left to right an Eller's maze.
	 */
	private void generateEller() { 	
		int deletex;
		int numWallBoardsDestroyed;
		Wallboard wall = new Wallboard(0,0,CardinalDirection.East);
		
		for(int heightIterator = height; heightIterator > 1; heightIterator--) {
			
			deletex = random.nextInt(width-1); // this determines which wall to destroy
			numWallBoardsDestroyed = random.nextInt(width-2); // number of walls to destroy
			wall.setLocationDirection(deletex,heightIterator-1,CardinalDirection.East);
			
			for (int i = 0; i <= numWallBoardsDestroyed; i++) { // each loop destroys one wallboard);
				
				if(floorplan.canTearDown(wall)) { // if you can tear it down,
					floorplan.deleteWallboard(wall); // tear it down
					mergeCells();
					wall.setLocationDirection(random.nextInt(width-1), heightIterator-1, CardinalDirection.East); // choose another spot
			}
				else { // if it tries to delete the same wall again, just try another spot for next iteration.
					wall.setLocationDirection(random.nextInt(width-1), heightIterator-1, CardinalDirection.East);
				}
				
			}
			
			
			// Step 2: Vertical connections south
			int currentSet;
			ArrayList<Integer> xList = new ArrayList<Integer>(); // list of x values within given set
			
			for(int i = 0; i < width; i++) { // for each cell in the row
				currentSet = cellsets.get(i)[2]; // gets Set ID 
				if(i > 0 && cellsets.get(i)[2] == cellsets.get(i-1)[2]) {
					continue;
				}
				
				for(int j = 0; j < width; j++) { //loop to compile list
					if(cellsets.get(j)[2] == currentSet) {
						xList.add(cellsets.get(j)[0]); // add x values of the same set to xList
					}	
				}
				
	
				int trueY = -1 * (cellsets.get(i)[1] - (heightIterator-1)); // flipped y values, with (0,0) at top left
				int randX;
				int randInt;
				int numOfConnections;  //number of connections for each indivdual set
				
				if(xList.size() > 1) {
					numOfConnections = random.nextInt(xList.size() + 1) + 1; // need between 1 and xList size, can't have 0
				}
				else { 
					numOfConnections = 1;
				}
				
				for(int k = 0; k < numOfConnections; k++) { //This is the loop which deletes walls
					
					randInt = random.nextInt(xList.size());
					randX  = xList.get(randInt); // choose random x from xlist
					wall.setLocationDirection(randX, trueY - 1, CardinalDirection.South);
					
					
					
					if(floorplan.canTearDown(wall)) {
						floorplan.deleteWallboard(wall);
						mergeCells();
						if(i < width-1) {
							if(cellsets.get(i)[2] == cellsets.get(i+1)[2] && xList.lastIndexOf(cellsets.get(i+1)[0]) != -1) {
								xList.remove(xList.lastIndexOf(cellsets.get(i+1)[0]));
							}
						}
						randX  = xList.get(random.nextInt(xList.size()));
						wall.setLocationDirection(randX, trueY - 1, CardinalDirection.South); //reset to possibly a new location
						
					}
					else {
						randX  = xList.get(random.nextInt(xList.size()));
						wall.setLocationDirection(randX, trueY - 1, CardinalDirection.South);
					}
				}
				
				
				xList.clear();
			}
		}
		
	lastRow();	
	}
	
	

	/**
	 *  Last row:
	 * Difference is instead of randomly connecting adjacents, connect all adjacent sets together (break all wallboards.)
	 * 
	 * So, create random connections to northern neighbors (second to last row), at least 1 per set 
	 * Instead of creating new sets for the non-connected cells in the last row, breakdown all wallBoards.
	 * If done properly, all cells should be in one set, and the ArrayList should only have one value stored, 
	 * a large set with every single cell.
	 */
	private void lastRow() {

		int bottomindex = 0;
		
		for(int i = 0; i < cellsets.size(); i++) {
			if(cellsets.get(i)[0] == 0 && cellsets.get(i)[1] == height-1) {
				bottomindex = i; // finding first spot of the bottom row √
			}
		}
		
		
		int firstset = cellsets.get(bottomindex)[2];
		
		for(int i = 0; i < cellsets.size(); i++) {
			System.out.println(Arrays.toString(cellsets.get(i)));
		}
		
		
		Wallboard wall = new Wallboard(0,height-1,CardinalDirection.East);
		
		for(int i = 0; i < width-1; i++) { //iterate through all bottom row cells, except the last one
			if(cellsets.get(i)[2] != cellsets.get(i+1)[2]) {
				floorplan.deleteWallboard(wall);
				mergeCells();
			}
		}
		
	}
	
	/**
	 * This method merges cells together after walls have been broken.
	 * Uses same traversal methods as the algorithm (top left is 0,0)
	 * Not sure why North works for vertical connection, but it does.
	 */
	private void mergeCells() {
		
		int trueY;
		for(int j = 0; j < cellsets.size(); j++) {
			trueY = -1 * (cellsets.get(j)[1] - (height-1));
			if(j < 12)  {
			if(floorplan.hasNoWall(cellsets.get(j)[0], trueY, CardinalDirection.North)) {
				cellsets.get(j+4)[2] = cellsets.get(j)[2];
				}
			}
		}
		for(int i = 0; i < cellsets.size(); i++) {
			trueY = -1 * (cellsets.get(i)[1] - (height-1));
			if(i  % 4 != 3)  { // not right row
				if(floorplan.hasNoWall(cellsets.get(i)[0], trueY, CardinalDirection.East))  {
					cellsets.get(i+1)[2] = cellsets.get(i)[2];
				}
			}
		}

	}
	
	
	public ArrayList<int[]> getCellSets() {
		return cellsets;
	}
	
}
	




	
