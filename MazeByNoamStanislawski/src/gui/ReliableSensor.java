package gui;

import generation.CardinalDirection;
import generation.Maze;
import generation.MazeContainer;
import gui.Robot.Direction;

/**
 * This is a reliable sensor, meaning that it never fails.
 * It relies upon the DistanceSensor interface and requires a 
 * given maze and robot onto which it is attached. 
 * It looks in one given direction relative to the robot,
 * as given from the state Direction sensorDirection.
 * It sense the distance to a given obstacle, and stores
 * that within its field distance.
 * @author Noam Stanislawski
 *
 */
class ReliableSensor implements DistanceSensor {
	private MazeContainer currentMaze;
	private ReliableRobot robot;
	private Direction sensorDirection;
	private int energyForSensing = 1;
	private int distance = 0;
	
	
	public ReliableSensor() {
		
	}

	@Override
	public int distanceToObstacle(int[] currentPosition, CardinalDirection currentDirection, float[] powersupply)
			throws Exception {
		
		
		robot.getCurrentPosition();
		CardinalDirection robotDirection = robot.getCurrentDirection(); // this is forward
		
		if(currentDirection.compareTo(robotDirection) == 0) {
			distance = robot.distanceToObstacle(Direction.FORWARD);
		}
		else if(currentDirection.compareTo(robotDirection.oppositeDirection()) == 0) {
			distance = robot.distanceToObstacle(Direction.BACKWARD);
		}
		else if(currentDirection.compareTo(robotDirection.rotateClockwise()) == 0) {
			distance = robot.distanceToObstacle(Direction.RIGHT);
		}
		else {
			distance = robot.distanceToObstacle(Direction.LEFT);
		}
		
		return distance;
	}

	@Override
	public void setMaze(Maze maze) {
		currentMaze = (MazeContainer) maze;
	}
	
	public void setRobot(ReliableRobot r) {
		robot = r;
	}

	@Override
	public void setSensorDirection(Direction mountedDirection) {
		sensorDirection = mountedDirection;

	}

	public float getEnergyConsumptionForSensing() {
		return energyForSensing;
	}

	@Override
	public void startFailureAndRepairProcess(int meanTimeBetweenFailures, int meanTimeToRepair)
			throws UnsupportedOperationException {
		System.out.println("ReliableSensor does not fail!");

	}

	@Override
	public void stopFailureAndRepairProcess() throws UnsupportedOperationException {
		System.out.println("ReliableSensor does not fail!");
	}

}
