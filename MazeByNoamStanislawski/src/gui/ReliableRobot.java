package gui;


import generation.CardinalDirection;
import generation.Floorplan;
import generation.MazeContainer;
import gui.Constants.UserInput;

/**
 * This class implements the Robot interface. It has multiple helper methods which keep 
 * track of the battery, the odometer, direction, and location of the robot. It also passes
 * info to a robotDriver, which is then able to solve the maze. This robot also has different sensors
 * which tell you if you are in a room, and if you are at the exit.
 * 
 * @author Noam Stanislawski
 *
 */
class ReliableRobot implements Robot {
	
	 private Controller robotController = new Controller();
	 private MazeContainer maze = (MazeContainer) robotController.getMazeConfiguration();
	 private Floorplan floorplan;

	 //private Distance distanceMatrix = maze.getMazedists();
	 
	 private CardinalDirection currentDirection;
	 private int[] currentPosition;
	
	 private float batteryLevel = 3500;
	 private int odometer = 0;
	 private int fullEnergyRotation = 12;
	 private int stepForwardEnergy = 6;
	 
	public ReliableRobot() {
	}
	
	
	@Override
	public void setController(Controller controller) {
		if(controller == null) {
			throw new IllegalArgumentException();
		}
		
		robotController = controller;
		maze = (MazeContainer) controller.getMazeConfiguration();
		//floorplan = maze.getFloorplan();
	}

	/**
	 * 
	 */
	@Override
	public int[] getCurrentPosition() throws Exception {
		currentPosition = robotController.getCurrentPosition();
		maze = (MazeContainer) robotController.getMazeConfiguration();
		floorplan = maze.getFloorplan();
		
		if(currentPosition[0] < maze.getWidth() && currentPosition[0] >= 0 && currentPosition[1] < maze.getHeight() && currentPosition[1] >= 0)
			return currentPosition;
		else {
			throw new Exception();
		}
		
		
	}

	@Override
	public CardinalDirection getCurrentDirection() {
		currentDirection = robotController.getCurrentDirection();
		return currentDirection;
	}

	@Override
	public float getBatteryLevel() {
		return batteryLevel;
	}

	@Override
	public void setBatteryLevel(float level) {
		if(level <= 0) {
			throw new IllegalArgumentException();
		}
		else {
			batteryLevel = level;
		}
		
	}

	@Override
	public float getEnergyForFullRotation() {
		return fullEnergyRotation;
	}

	@Override
	public float getEnergyForStepForward() {
		return stepForwardEnergy;
	}

	@Override
	public int getOdometerReading() {
		return odometer;
	}

	@Override
	public void resetOdometer() {
		odometer = 0;

	}

	@Override
	public void rotate(Turn turn) {
		if(!hasStopped()) {
			if(turn == Turn.LEFT) {
				robotController.keyDown(UserInput.Left,0);
				batteryLevel -= 3;
			}
			else if(turn == Turn.RIGHT) {
				robotController.keyDown(UserInput.Right,0);
				batteryLevel -= 3;
			}
			else if(turn == Turn.AROUND) {
				robotController.keyDown(UserInput.Left,0);
				robotController.keyDown(UserInput.Left,0);
				batteryLevel -= 6;
			}
		}
			currentDirection = robotController.getCurrentDirection();
		

	}

	@Override
	public void move(int distance) {
		for(int i = 0; i < distance;i++) {
			if(!hasStopped()) {
				robotController.keyDown(UserInput.Up,0);
				odometer++;
				batteryLevel -= stepForwardEnergy;
				currentDirection = robotController.getCurrentDirection();
				
			}
		}
	}

	@Override
	public void jump() {
		if(!hasStopped()) {
			robotController.keyDown(UserInput.Jump,0);
			odometer++;
			batteryLevel -= 40;
			currentDirection = robotController.getCurrentDirection();
			
		}

	}

	@Override
	public boolean isAtExit() {
		currentPosition = robotController.getCurrentPosition();
		maze = (MazeContainer) robotController.getMazeConfiguration();
		floorplan = maze.getFloorplan();
		
		if(floorplan.isExitPosition(currentPosition[0], currentPosition[1])) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean isInsideRoom() {
		try {
			currentPosition = getCurrentPosition();
		} catch (Exception e) {
			
		}
		if(floorplan.isInRoom(currentPosition[0], currentPosition[1])) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean hasStopped() {
		if(batteryLevel > 0 || distanceToObstacle(Direction.FORWARD) == 0) {
			return false;
		}
		else {
			return true;
		}
	}

	
	@Override
	public int distanceToObstacle(Direction direction) throws ArrayIndexOutOfBoundsException {
		maze = (MazeContainer) robotController.getMazeConfiguration();
		floorplan = maze.getFloorplan();
		currentPosition = robotController.getCurrentPosition(); //don't need to throw exception 
		currentDirection = robotController.getCurrentDirection();
		
		
		int distance = 0;
		int distx = currentPosition[0];
		int disty = currentPosition[1];
		
		try {
			while(!floorplan.hasWall(distx, disty, currentDirection)) { //runs until finds a way
			
				if(floorplan.isExitPosition(currentPosition[0], currentPosition[1])) { // at the exit
					if(!floorplan.hasWall(currentPosition[0], currentPosition[1], currentDirection)) { // facing a wall
						return Integer.MAX_VALUE;
						
					}
				}
				
				if(direction == Direction.FORWARD) {
					if(currentDirection == CardinalDirection.North) { //checking =north
						disty--; 
						
					}
					
					else if(currentDirection == CardinalDirection.South) {
						disty++; 
						
					}
					
					else if(currentDirection == CardinalDirection.East) {
						distx++;
						
					}
					
					else if(currentDirection == CardinalDirection.West) {
						distx--;
					}
				}
				if(direction == Direction.BACKWARD ) {
					if(currentDirection == CardinalDirection.North) { //going south
						disty++;
					}
					
					else if(currentDirection == CardinalDirection.South) { //going north
						disty--;
					}
					
					else if(currentDirection == CardinalDirection.East) { //going west
						distx--;
					}
					
					else if(currentDirection == CardinalDirection.West) { //going east
						distx++;
					}
					
				}
				if(direction == Direction.LEFT ) {
					if(currentDirection == CardinalDirection.North) { //going east
						distx++;
					}
					
					else if(currentDirection == CardinalDirection.South) { //going west
						distx--;
					}
					
					else if(currentDirection == CardinalDirection.East) { // going south
						disty++;
					}
					
					else if(currentDirection == CardinalDirection.West) { //going north
						disty--;
					}
					
				}
				if(direction == Direction.RIGHT) {
					if(currentDirection == CardinalDirection.North) { // going west
						distx--;
					}
					
					else if(currentDirection == CardinalDirection.South) { // going east
						distx++;
					}
					
					else if(currentDirection == CardinalDirection.East) { // going north
						disty--;
					}
					
					else if(currentDirection == CardinalDirection.West) { // going south
						disty++;
					}
					
				}
				
				distance++;
			}
		}
		
		catch(ArrayIndexOutOfBoundsException e) {	
			System.out.println("dont do that");
			return distance;
		}
		
		
		return distance;
	}

	@Override
	public boolean canSeeThroughTheExitIntoEternity(Direction direction) throws UnsupportedOperationException {
		maze = (MazeContainer) robotController.getMazeConfiguration();
		floorplan = maze.getFloorplan();
		currentPosition = robotController.getCurrentPosition(); //don't need to throw exception 
		currentDirection = getCurrentDirection();
		if(distanceToObstacle(Direction.FORWARD) == Integer.MAX_VALUE) {
			return true;
		}
		else {
			return false;
		}

	}


	@Override
	public void startFailureAndRepairProcess(Direction direction, int meanTimeBetweenFailures, int meanTimeToRepair)
			throws UnsupportedOperationException {
		// TODO Auto-generated method stub

	}

	@Override
	public void stopFailureAndRepairProcess(Direction direction) throws UnsupportedOperationException {
		// TODO Auto-generated method stub

	}

}
