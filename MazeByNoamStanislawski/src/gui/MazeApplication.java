/**
 * 
 */
package gui;

import generation.Order;

import java.awt.event.KeyListener;
import java.io.File;
import java.util.Arrays;
import java.util.List;

import javax.swing.JFrame;


/**
 * This class is a wrapper class to startup the Maze game as a Java application
 * 
 * This code is refactored code from Maze.java by Paul Falstad, www.falstad.com, Copyright (C) 1998, all rights reserved
 * Paul Falstad granted permission to modify and use code for teaching purposes.
 * Refactored by Peter Kemper
 * 
 * TODO: use logger for output instead of Sys.out
 */
public class MazeApplication extends JFrame {

	// not used, just to make the compiler, static code checker happy
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 */
	public MazeApplication() {
		init(null);
	}

	/**
	 * Constructor that loads a maze from a given file or uses a particular method to generate a maze
	 * @param parameter can identify a generation method (Prim, Kruskal, Eller)
     * or a filename that stores an already generated maze that is then loaded, or can be null
	 */
	public MazeApplication(String[] parameter) {
		init(parameter);
	}

	/**
	 * Instantiates a controller with settings according to the given parameter.
	 * 
	 * @param mazeType can identify a generation method (Prim, Kruskal, Eller)
	 * or a filename that contains a generated maze that is then loaded,
	 * or can be null.
	 * 
	 * @param driver can identify a driver method (Wizard, WallFollower, Manual)
	 * or it can be null.
	 * 
	 * @param sensors identifies which sensors are reliable or unreliable.
	 * Orientation is front, left, back, right, with 1 = reliable, 0 = unreliable.
	 * 
	 * @return the newly instantiated and configured controller
	 */
	 Controller createController(String mazeType, String driver, String sensors) {
	    // need to instantiate a controller to return as a result in any case
	    Controller result = new Controller() ;
	    
	    // can decide if user repeatedly plays the same mazes or 
	    // if mazes are different each and every time
	    // set to true for testing purposes
	    // set to false for playing the game
	    result.setDeterministic(true);
	    String msg = null; // message for feedback
	    
	    // Case 1: no input
	    if ("".equalsIgnoreCase(mazeType) || "DFS".equalsIgnoreCase(mazeType)) {
	        msg = "MazeApplication: maze will be generated with a randomized algorithm."; 
	    }
	    // Case 2: Prim
	    else if ("Prim".equalsIgnoreCase(mazeType))
	    {
	        msg = "MazeApplication: generating random maze with Prim's algorithm.";
	        result.setBuilder(Order.Builder.Prim);
	    }
	    // Case 3 a and b: Eller, Kruskal or some other generation algorithm
	    else if ("Kruskal".equalsIgnoreCase(mazeType))
	    {
	        msg = "MazeApplication: generating random maze with Kruskal's algorithm.";
	        result.setBuilder(Order.Builder.Kruskal);
	    }
	    else if ("Eller".equalsIgnoreCase(mazeType))
	    {
	    	msg = "MazeApplication: generating random maze with Eller's algorithm.";
	        result.setBuilder(Order.Builder.Eller);
	    }
	    
	    // Case 4: a file
	    else if(!"Manual".equalsIgnoreCase(mazeType) && !"".equals(mazeType)) {
	    	mazeType = (String) mazeType;
	        File f = new File(mazeType) ;
	        if (f.exists() && f.canRead())
	        {
	            msg = "MazeApplication: loading maze from file: " + mazeType;
	            result.setFileName(mazeType);
	        }
	        else {
	            // None of the predefined strings and not a filename either: 
	            msg = "MazeApplication: unknown parameter value: " + mazeType + " ignored, operating in default mode.";
	        }
	    }
	    
	    // Case 5: Running Wizard & WallFollower
	    if ("Wizard".equalsIgnoreCase(driver))
	    {
	    	msg += " The maze is being driven by the Wizard driver.";
	    	ReliableRobot r = new ReliableRobot();
	    	Wizard wizard = new Wizard();
	    	wizard.setRobot(r);
	    	result.setRobotAndDriver(r, wizard); 
	    	result.driverChoice = "Wizard";
	    }
	    
	    else if ("WallFollower".equalsIgnoreCase(driver))
	    {
	    	msg += " The maze is being driven by the WallFollower driver.";
	    	ReliableRobot r = new ReliableRobot();
	    	WallFollower wallfollower = new WallFollower();
	    	wallfollower.setRobot(r);
	    	result.setRobotAndDriver(r, wallfollower); 
	    	result.driverChoice = "WallFollower";
	    }
	    
	    // Case 7: Running Manual driver
	    else if ("Manual".equalsIgnoreCase(driver) || "".equalsIgnoreCase(driver))
	    {
	    	msg += " The maze is being driven manually.";
	    	result.setRobotAndDriver(null, null);
	    	result.driverChoice = "";

	    }
	    
	    //Case 8: Sensors
	    int[] sensorArray = new int[4];
	    System.out.println(sensors.toString());
	    for(int i = 0; i < sensors.length(); i++) {
	  
	    	sensorArray[i] = sensors.charAt(i);
	    }
	    result.UnreliableSensorsArray = sensorArray;

	    
	    // controller instanted and attributes set according to given input parameter
	    // output message and return controller
	    System.out.println(msg);
	    return result;
	    	
	    }


	/**
	 * Initializes some internals and puts the game on display.
	 * @param parameter can identify a generation method (Prim, Kruskal, Eller)
     * or a filename that contains a generated maze that is then loaded, or can be null
	 */
	private void init(String[] parameter) {
		System.out.println(Arrays.toString(parameter));
		List<String> parameterList = Arrays.asList(parameter);
		
		// Set default values. All sensor should be reliable by default.
		String mazeType = "";
		String driver = "";
		String sensors = "1111";
		
		// checks for a set type of maze algorithm
		if(parameterList.contains("-g")) {
			int gameIndex = parameterList.indexOf("-g") + 1;
			mazeType = parameterList.get(gameIndex);
		}
		
		// check for a set type of driver algorithm
		if(parameterList.contains("-d")) {
			int driverIndex = parameterList.indexOf("-d") + 1;
			driver = parameterList.get(driverIndex);
		}
		
		// check if any sensors are unreliable
		if(parameterList.get(parameterList.size()-1).contains("0")) {
			sensors = parameterList.get(parameterList.size()-1);
		}
		
		
		// instantiate a game controller and add it to the JFrame
	    Controller controller = createController(mazeType, driver, sensors);
		add(controller.getPanel()) ;
		
		// instantiate a key listener that feeds keyboard input into the controller
		// and add it to the JFrame
		KeyListener kl = new SimpleKeyListener(this, controller) ;
		addKeyListener(kl) ;
		
		// set the frame to a fixed size for its width and height and put it on display
		setSize(Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT+22) ;
		setVisible(true) ;
		
		// focus should be on the JFrame of the MazeApplication and not on the maze panel
		// such that the SimpleKeyListener kl is used
		setFocusable(true) ;
		
		// start the game, hand over control to the game controller
		controller.start();
	}
	
	/**
	 * Main method to launch Maze game as a java application.
	 * The application can be operated in three ways. 
	 * 1) The intended normal operation is to provide no parameters
	 * and the maze will be generated by a randomized DFS algorithm (default). 
	 * 2) If a filename is given that contains a maze stored in xml format. 
	 * The maze will be loaded from that file. 
	 * This option is useful during development to test with a particular maze.
	 * 3) A predefined constant string is given to select a maze
	 * generation algorithm, currently supported is "Prim".
	 * 
	 * @param args lets you set the maze algorithm, maze solver aglorithm, and number of UnreliableSensors.
	 * If you use no parameters, the game will default to a manual DFS game.
	 * First parameter will be -g "BuilderType" -> either Eller, Prim, or DFS.
	 * Second parameter will be -d "driverType" -> either Manual, Wizard, Wallfollower.
	 * Finally, you can add a 4 digit integer which represents:
	 * forward sensor, left sensor, right sensor, and backward sensor. 
	 * 
	 */
	
	public static void main(String[] args) {
	    JFrame app ; 
	    if(args.length > 0) {
	    	app = new MazeApplication(args);
		}
	    else {
	    	app = new MazeApplication();
	    }
		app.repaint() ;
	}

}
