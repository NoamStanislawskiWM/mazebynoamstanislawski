package gui;

import generation.CardinalDirection;
import generation.Floorplan;
import generation.Maze;
import gui.Robot.Direction;
import gui.Robot.Turn;

class WallFollower implements RobotDriver {

	Robot robot;
	Maze curMaze;
	int energyConsumption;
	int pathLength;
	Floorplan floorplan;
	CardinalDirection curDirecton;
	
	ReliableSensor leftSensor = new ReliableSensor();
	ReliableSensor forwardSensor = new ReliableSensor();
	ReliableSensor rightSensor = new ReliableSensor();
	float[] powersupply;	
	
	public WallFollower() {
	}

	@Override
	public void setRobot(Robot r) {
		robot = r;
		leftSensor.setRobot((ReliableRobot) robot);
		rightSensor.setRobot((ReliableRobot) robot);
		forwardSensor.setRobot((ReliableRobot) robot);

	}

	@Override
	public void setMaze(Maze maze) {
		curMaze = maze;
		leftSensor.setMaze(curMaze);
		forwardSensor.setMaze(curMaze);
		rightSensor.setMaze(curMaze);

	}

	@Override
	public boolean drive2Exit() throws Exception {
		
		while(!robot.isAtExit() && !robot.hasStopped()) {
			curDirecton = robot.getCurrentDirection();
			drive1Step2Exit();
			if(robot.getBatteryLevel() <= 0) {
				throw new Exception();
			}	
		}
		
//		while(robot.distanceToObstacle(Direction.FORWARD) < Integer.MAX_VALUE) {
//			robot.rotate(Turn.LEFT);
//		}
//		robot.move(1);
		
		return true;

	}
	/**
	 * Need to account for 2 types of robot
	 * @throws Exception 
	 */
	@Override
	public boolean drive1Step2Exit() throws Exception {
		powersupply = new float[]{robot.getBatteryLevel()};
		leftSensor.setSensorDirection(Direction.LEFT);
		forwardSensor.setSensorDirection(Direction.FORWARD);
		rightSensor.setSensorDirection(Direction.RIGHT);
		
		int leftDist = robot.distanceToObstacle(Direction.LEFT);
		int rightDist = robot.distanceToObstacle(Direction.RIGHT);
		int forwardDist = robot.distanceToObstacle(Direction.FORWARD);

		if(!(robot instanceof UnreliableRobot)) { // if robot is reliable, need negation bc UnreliableRobot is subclass of ReliableRobot
			
			int[] currentPos = robot.getCurrentPosition();
			if(leftDist == 0) { // there is a left wall	;
				if(forwardDist > 0) { //proceed normally
					robot.move(1);
					return true;
				}
				else if(forwardDist == 0 && rightDist != 0) { // must turn right
					System.out.println("WORKING");
					robot.rotate(Turn.RIGHT);
					robot.move(1);
					return true;
				}
			}
			else if(leftDist > 0) {
				robot.rotate(Turn.LEFT);
				robot.move(1);
				return true;
			}
		
		}
		
//		else if(robot instanceof UnreliableRobot) {
//			
//			
//		}
		return false;
	}

	@Override
	public float getEnergyConsumption() {
		return 3500 - robot.getBatteryLevel();
	}

	@Override
	public int getPathLength() {
		return robot.getOdometerReading();
	}

}
