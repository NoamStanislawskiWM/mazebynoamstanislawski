package gui;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Panel;
import java.awt.RenderingHints;
import java.awt.font.GlyphVector;
import java.awt.geom.Rectangle2D;
import java.awt.Color;
import java.awt.Font;

/**
 * Add functionality for double buffering to an AWT Panel class.
 * Used for drawing a maze.
 * 
 * @author Peter Kemper
 *
 */
public class MazePanel extends Panel implements P5Panel  {
	private static final long serialVersionUID = 2787329533730973905L;
	/* Panel operates a double buffer see
	 * http://www.codeproject.com/Articles/2136/Double-buffer-in-standard-Java-AWT
	 * for details
	 */
	// bufferImage can only be initialized if the container is displayable,
	// uses a delayed initialization and relies on client class to call initBufferImage()
	// before first use
	private Image bufferImage;  
	private Graphics2D graphics; // obtained from bufferImage, 
	// graphics is stored to allow clients to draw on the same graphics object repeatedly
	// has benefits if color settinsgs should be remembered for subsequent drawing operations
	//private int color; //stores argb value used for coloring. 
	
	
	// markerFont is the font used by the compassRose, and in the addMarker() method.
	// color stores the current color value of the mazePanel.
	private Font markerFont = Font.decode("Serif-PLAIN-16");
	private Color color;
	
	// These are the colors used by the maze, represented in their rgb integer value.
	// highlight and shading colors cannot be integers as they contain alpha values, so they remain Color objects.
	static final int MARKER_COLOR = 0;
    static final Color CIRCLE_HIGHLIGHT = new Color(1.0f, 1.0f, 1.0f, 0.8f); 
    static final Color CIRCLE_SHADE = new Color(1.0f, 1.0f, 1.0f, 0.3f); //new Color(0.0f, 0.0f, 0.0f, 0.2f); 
	static final int  greenWM = 1136448;
	static final int  goldWM = 9531201; 
	static final int yellowWM = 16777113;
	static final int lightGray = 13882323;
	static final int white = 16777215;
	static final int yellow = 16776960;
	static final int gray = 8421504;
	static final int red = 16711680;
	
	
	/**
	 * Constructor. Object is not focusable.
	 */
	public MazePanel() {
		setFocusable(false);
		bufferImage = null; // bufferImage initialized separately and later
		graphics = null;	// same for graphics
		color = new Color(white); // default of black
	}
	
	
	/**
	 * Draws the buffer image to the given graphics object.
	 * This method is called when this panel should redraw itself.
	 * The given graphics object is the one that actually shows 
	 * on the screen.
	 */
	@Override
	public void paint(Graphics g) {
		if (null == g) {
			System.out.println("MazePanel.paint: no graphics object, skipping drawImage operation");
		}
		else {
			g.drawImage(bufferImage,0,0,null);	
		}
	}
	
	/**
	 * Obtains a graphics object that can be used for drawing.
	 * This MazePanel object internally stores the graphics object 
	 * and will return the same graphics object over multiple method calls. 
	 * The graphics object acts like a notepad where all clients draw 
	 * on to store their contribution to the overall image that is to be
	 * delivered later.
	 * To make the drawing visible on screen, one needs to trigger 
	 * a call of the paint method, which happens 
	 * when calling the update method. 
	 * @return graphics object to draw on, null if impossible to obtain image
	 */
	public Graphics getBufferGraphics() {
		// if necessary instantiate and store a graphics object for later use
		if (!isOperational()) { 
			if (null == bufferImage) {
				bufferImage = createImage(Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
				if (null == bufferImage)
				{
					System.out.println("Error: creation of buffered image failed, presumedly container not displayable");
					return null; // still no buffer image, give up
				}		
			}
			graphics = (Graphics2D) bufferImage.getGraphics();
			if (null == graphics) {
				System.out.println("Error: creation of graphics for buffered image failed, presumedly container not displayable");
			}
		}
		else {
			// For drawing in FirstPersonDrawer, setting rendering hint
			// became necessary when lines of polygons 
			// that were not horizontal or vertical looked ragged
			graphics.setRenderingHint(java.awt.RenderingHints.KEY_ANTIALIASING, java.awt.RenderingHints.VALUE_ANTIALIAS_ON);
			graphics.setRenderingHint(java.awt.RenderingHints.KEY_INTERPOLATION, java.awt.RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			}
		
		return graphics;
	}


	@Override
	public void commit() {
		paint(getGraphics());
		
	}
	
	public void commit(Graphics g)  {
		paint(g);
	}

	@Override
	public boolean isOperational() {
		if (null == graphics) { 
			
			if (null == bufferImage) {
				bufferImage = createImage(Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
				if (null == bufferImage)
				{
					System.out.println("NULL IMAGE REMAINS NULL");
					return false; // still no buffer image, give up
				}		
			}
			
			graphics = (Graphics2D) bufferImage.getGraphics();
			if (null == graphics) {
				System.out.println("NULL GRAPHICS REMANINS FAILED");
				return false;
			}
		}
	return true;
	}

	@Override
	public void setColor(int rgb) {
		color = new Color(rgb);
		graphics.setColor(color);
		
	}
	
	public Font getFont() {
		return markerFont;
	}

	public void setFont(Font f) {
		markerFont = f;
	}
	
	@Override
	public int getColor() {
		return color.getRGB();
	}
	
	public int getRed() {
		int red = color.getRed();
		return red;
	}

	public int getGreen() {
		int green = color.getGreen();
		return green;
		
	}
	
	public int getBlue() {
		int blue = color.getBlue();
		return blue;
	}
	
	@Override
	public int getWallColor(int distance, int cc, int extensionX) {
		distance /= 4;
        final int part1 = distance & 7;
        final int add = (extensionX != 0) ? 1 : 0;
        final int rgbValue = ((part1 + 2 + add) * 70) / 8 + 80;
        return rgbValue;
	}

	@Override
	public void addBackground(float percentToExit) {
		
		graphics.setColor(getBackgroundColor(percentToExit, true));
		addFilledRectangle(0, 0, Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT/2);
		// grey rectangle in lower half of screen
		// graphics.setColor(Color.darkGray);
		// dynamic color setting: 
		graphics.setColor(getBackgroundColor(percentToExit, false));
		addFilledRectangle(0, Constants.VIEW_HEIGHT/2, Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT/2);
		
	}
	
	void setColor(Color c) {
		color = c;
		graphics.setColor(color);
		
	}


	/**
	 * Determine the background color for the top and bottom
	 * rectangle as a blend between starting color settings
	 * of black and grey towards gold and green as final
	 * color settings close to the exit
	 * @param percentToExit
	 * @param top is true for the top triangle, false for the bottom
	 * @return the color to use for the background rectangle
	 */
	private Color getBackgroundColor(float percentToExit, boolean top) {
		return top? blend(new Color(yellowWM), new Color(goldWM), percentToExit) : 
			blend(Color.lightGray, new Color(greenWM), percentToExit);
	}
	
	/**
	 * Calculates the weighted average of the two given colors
	 * @param c0 the one color
	 * @param c1 the other color
	 * @param weight0 of c0
	 * @return blend of colors c0 and c1 as weighted average
	 */
	private Color blend(Color c0, Color c1, double weight0) {
		if (weight0 < 0.1)
			return c1;
		if (weight0 > 0.95)
			return c0;
	    double r = weight0 * c0.getRed() + (1-weight0) * c1.getRed();
	    double g = weight0 * c0.getGreen() + (1-weight0) * c1.getGreen();
	    double b = weight0 * c0.getBlue() + (1-weight0) * c1.getBlue();
	    double a = Math.max(c0.getAlpha(), c1.getAlpha());

	    return new Color((int) r, (int) g, (int) b, (int) a);
	  }
	

	@Override
	public void addFilledRectangle(int x, int y, int width, int height) {
		graphics.fillRect(x, y, width, height);
		
		
		
	}

	@Override
	public void addFilledPolygon(int[] xPoints, int[] yPoints, int nPoints) {
		graphics.fillPolygon(xPoints, yPoints, nPoints);
		
	}

	@Override
	public void addPolygon(int[] xPoints, int[] yPoints, int nPoints) {
		if(isOperational()) {
			graphics.drawPolygon(xPoints, yPoints, nPoints);
		}
	}

	@Override
	public void addLine(int startX, int startY, int endX, int endY) {

		if(isOperational()) {
			graphics.drawLine(startX, startY, endX, endY);
		}
		
		else {
			bufferImage = createImage(Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
			graphics = (Graphics2D) bufferImage.getGraphics();
			graphics.drawLine(startX, startY, endX, endY);
		}
		
	}

	@Override
	public void addFilledOval(int x, int y, int width, int height) {
		if(isOperational()) {
			graphics.fillOval(x, y, width, height);
		}
		
		else {
			bufferImage = createImage(Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
			graphics = (Graphics2D) bufferImage.getGraphics();
			graphics.fillOval(x, y, width, height);
		}
		
	}

	@Override
	public void addArc(int x, int y, int width, int height, int startAngle, int arcAngle) {
		if(isOperational()) {
			graphics.drawArc(x, y, width, height, startAngle, arcAngle);
		}
		
		else {
			bufferImage = createImage(Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
			graphics = (Graphics2D) bufferImage.getGraphics();
			graphics.drawArc(x, y, width, height, startAngle, arcAngle);
		}
		
	}

	@Override
	public void addMarker(float x, float y, String str) {
        GlyphVector gv = markerFont.createGlyphVector(graphics.getFontRenderContext(), str);
        Rectangle2D rect = gv.getVisualBounds();
        
        x -= rect.getWidth() / 2;
        y += rect.getHeight() / 2;
        
        graphics.drawGlyphVector(gv, x, y);
		
	}

	@Override
	public void setRenderingHint(RenderingHints hintKey, RenderingHints hintValue) {
		
		if (hintKey == RenderingHints.KEY_ANTIALIASING) {
			if(hintValue == RenderingHints.VALUE_ANTIALIAS_ON) {
				graphics.setRenderingHint(java.awt.RenderingHints.KEY_ANTIALIASING, java.awt.RenderingHints.VALUE_ANTIALIAS_ON);
			}
		}
		
		if (hintKey == RenderingHints.KEY_INTERPOLATION) {
			if(hintValue == RenderingHints.VALUE_INTERPOLATION_BILINEAR) {
				graphics.setRenderingHint(java.awt.RenderingHints.KEY_INTERPOLATION, java.awt.RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			}
		}
		
		if (hintKey == RenderingHints.KEY_RENDERING) {
			if(hintValue == RenderingHints.VALUE_RENDER_QUALITY) {
				graphics.setRenderingHint(java.awt.RenderingHints.KEY_RENDERING, java.awt.RenderingHints.VALUE_RENDER_QUALITY);
			}
		}
	}

}
