package gui;

import generation.CardinalDirection;
import generation.Floorplan;
import generation.Maze;
import gui.Robot.Direction;
import gui.Robot.Turn;

class Wizard implements RobotDriver {

	Robot robot;
	Maze curMaze;
	int energyConsumption;
	int pathLength;
	Floorplan floorplan;
	
	public Wizard() {
	}

	@Override
	public void setRobot(Robot r) {
		robot = r;

	}

	@Override
	public void setMaze(Maze maze) {
		curMaze = maze;
		floorplan = maze.getFloorplan();

	}

	@Override
	public boolean drive2Exit() throws Exception {

		while(!robot.isAtExit() && !robot.hasStopped()) {
			drive1Step2Exit();
			if(robot.getBatteryLevel() <= 0) {
				throw new Exception();
			}	
		}
		
		// This ennsures that wizard goes through the exit, albeit an imperfect solution.
		// It tries every direction once it is at the exit in order to go through it
		// Usually there are some extra inputs but the time between them is so tiny
		// that is not a big deal.
		if(robot.distanceToObstacle(Direction.FORWARD) != Integer.MAX_VALUE && 
				curMaze.isValidPosition(robot.getCurrentPosition()[0], robot.getCurrentPosition()[1])) {
			robot.rotate(Turn.RIGHT);
			robot.move(1);
			robot.rotate(Turn.LEFT);
			
			robot.rotate(Turn.LEFT);
			robot.move(1);
			robot.rotate(Turn.RIGHT);
			
			robot.rotate(Turn.AROUND);
			robot.move(1);
			robot.rotate(Turn.AROUND);
			
			
			
		}
		
		
		return true;
		

	}

	@Override
	public boolean drive1Step2Exit() throws Exception {
		int[] curLocation = robot.getCurrentPosition();
		CardinalDirection curDirection = robot.getCurrentDirection();
		int[] nextLocation = curMaze.getNeighborCloserToExit(curLocation[0], curLocation[1]);
		if(robot.getBatteryLevel() > 0) {
			if(curLocation[0] == nextLocation[0]) { // moving north or south
				if(curLocation[1] > nextLocation[1]) { // moving north (downward)
					if(curDirection == CardinalDirection.North) { // facing right way
						robot.move(1);
						return true;
					}
					else if(curDirection == CardinalDirection.South) {
						robot.rotate(Turn.AROUND);
						robot.move(1);
						return true;
					}
					else if(curDirection == CardinalDirection.West) {
						robot.rotate(Turn.LEFT);
						robot.move(1);
						return true;
					}
					else if(curDirection == CardinalDirection.East) {
						robot.rotate(Turn.RIGHT);
						robot.move(1);
						return true;
					}
				}
				if(curLocation[1] < nextLocation[1]) { // moving south
					if(curDirection == CardinalDirection.South) { // facing right way
						robot.move(1);
						return true;
					}
					else if(curDirection == CardinalDirection.North) {
						robot.rotate(Turn.AROUND);
						robot.move(1);
						return true;
					}
					else if(curDirection == CardinalDirection.West) {
						robot.rotate(Turn.RIGHT);
						robot.move(1);
						return true;
					}
					else if(curDirection == CardinalDirection.East) {
						robot.rotate(Turn.LEFT);
						robot.move(1);
						return true;
					}			
				}
			}
			else if(curLocation[1] == nextLocation[1]) { //moving east or west
				if(curLocation[0] < nextLocation[0]) { // moving east
					if(curDirection == CardinalDirection.East) { // facing right way
						robot.move(1);
						return true;
					}
					else if(curDirection == CardinalDirection.West) {
						robot.rotate(Turn.AROUND);
						robot.move(1);
						return true;
					}
					else if(curDirection == CardinalDirection.South) {
						robot.rotate(Turn.RIGHT);
						robot.move(1);
						return true;
					}
					else if(curDirection == CardinalDirection.North) {
						robot.rotate(Turn.LEFT);
						robot.move(1);
						return true;
					}	
				}
				else if(curLocation[0] > nextLocation[0]) { // moving west
					if(curDirection == CardinalDirection.West) { // facing right way
						robot.move(1);
						return true;
					}
					else if(curDirection == CardinalDirection.East) {
						robot.rotate(Turn.AROUND);
						robot.move(1);
						return true;
					}
					else if(curDirection == CardinalDirection.North) {
						robot.rotate(Turn.RIGHT);
						robot.move(1);
						return true;
					}
					else if(curDirection == CardinalDirection.South) {
						robot.rotate(Turn.LEFT);
						robot.move(1);
						return true;
					}		
				}
			}
		}
		return false;
	}

	@Override
	public float getEnergyConsumption() {
		return 3500 - robot.getBatteryLevel();
	}

	@Override
	public int getPathLength() {
		return robot.getOdometerReading();
	}

}
