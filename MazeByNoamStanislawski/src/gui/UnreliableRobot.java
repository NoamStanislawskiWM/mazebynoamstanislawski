package gui;

import gui.Robot.Direction;

/**
 * UnreliableRobot is extremely similar to ReliableRobot, save for the sensors.
 * It is able to handle which sensors are unreliable as dictated from the controller,
 * and it also contains how long a sensor remains up or how long it takes to repair.
 * 
 * It mainly interacts with the Controller to recieve the new sensors,
 * but it also interacts with the maze and floorplan similarly to ReliableRobot in order 
 * to control itself.
 * 
 * There are four main fields in the method, which are the four differing sensors.
 * Each of them start out as ReliableSensors, and are changed to unreliable as needed.
 * The controller will then start their threads.
 * 
 * @author Noam Stanislawski
 *
 */
class UnreliableRobot extends ReliableRobot {
	protected ReliableSensor forwardSensor = new ReliableSensor();
	protected ReliableSensor leftSensor = new ReliableSensor();
	protected ReliableSensor rightSensor = new ReliableSensor();
	protected ReliableSensor backSensor = new ReliableSensor();
	
	protected int repairTime = 2000; // in milliseconds
	protected int upTime = 4000; //in milliseconds
	
	
	public UnreliableRobot() {
		super();
	}
	
	@Override
	public int distanceToObstacle(Direction direction) throws ArrayIndexOutOfBoundsException { 
		int distance = super.distanceToObstacle(direction);
		return distance;
		
	}
	
	public int getTimeToRepair() {
		return repairTime;
	}
	
	public int getUpTime() {
		return upTime;
	}
	
	public void setUnreliableSensor(UnreliableSensor newSensor, Direction direction) {
		if(direction == Direction.FORWARD) {
			forwardSensor = newSensor;
		}
		if(direction == Direction.BACKWARD) {
			backSensor = newSensor;
			
		}
		if(direction == Direction.LEFT) {
			leftSensor = newSensor;
			
		}
		if(direction == Direction.RIGHT) {
			rightSensor = newSensor;
			
		}
	}
	
//	public boolean isSensorFunctional(UnreliableSensor sensor) {
//		if(sensor== Thread.State.TIMED_WAITING)
//	}

}
