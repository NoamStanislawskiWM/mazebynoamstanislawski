package gui;

import javax.swing.JComponent;
import generation.CardinalDirection;

/**
 * A component that draws a compass rose.  This class has no other functionality, but superclasses
 * may add functionality to it.
 * 
 * @author Sampo Niskanen <sampo.niskanen@iki.fi>
 * Code copied from http://www.soupwizard.com/openrocket/code-coverage/eclemma-20121216/OpenRocket/src/net.sf.openrocket.gui.components.compass/CompassRose.java.html
 * adjusted for Maze setting by
 * @author Peter Kemper
 */
public class CompassRose extends JComponent {
	private static final long serialVersionUID = 1916497172430988388L;
//	private static final Color greenWM = Color.decode("#115740");
//	private static final Color goldWM = Color.decode("#916f41");
	

    private static final int MAIN_COLOR =  6711039; //rgb color
    private static final float MAIN_LENGTH = 0.95f;
    private static final float MAIN_WIDTH = 0.15f;
    
    private static final int CIRCLE_BORDER = 2;
//    
//    private static final Color MARKER_COLOR = Color.black; //Color.WHITE; //Color.BLACK;
    

    private double scaler;
    
    private double markerRadius;
//    private Font markerFont;
    
    // (x,y) coordinates of center point on overall area
    private int centerX; // x coordinate of center point
    private int centerY; // y coordinate of center point
    private int size; // size of compass rose
    private CardinalDirection currentDir; 
    private MazePanel panel;
    
    /**
     * Construct a compass rose with the default settings.
     */
    public CompassRose() {
        this(0.9, 1.7);
    }
    
    
    /**
     * Construct a compass rose with the specified settings.
     * 
     * @param scaler        The scaler of the rose.  The bordering circle will be this portion of the component dimensions.
     * @param markerRadius  The radius for the marker positions (N/E/S/W), or NaN for no markers.  A value greater than one
     *                      will position the markers outside of the bordering circle.
     * @param markerFont    The font used for the markers.
     */
    public CompassRose(double scaler, double markerRadius) {
        this.scaler = scaler;
        this.markerRadius = markerRadius;
    }
    
    public void setPositionAndSize(int x, int y, int size) {
    	centerX = x;
    	centerY = y;
    	this.size = size;
    }
    /**
     * Set the current direction such that it can
     * be highlighted on the display
     * @param cd
     */
    public void setCurrentDirection(CardinalDirection cd) {
    	currentDir = cd;
    }

    public void paintComponent(MazePanel p) {
        
        final MazePanel p2 = p;
        
        
        /* Original code
        Dimension dimension = this.getSize();
        int width = Math.min(dimension.width, dimension.height);
        int mid = width / 2;
        width = (int) (scaler * width);
        */
        int width = (int) (scaler * size);
        final int mainLength = (int) (width * MAIN_LENGTH / 2);
        final int mainWidth = (int) (width * MAIN_WIDTH / 2);
        
        p2.setRenderingHint(P5Panel.RenderingHints.KEY_RENDERING, P5Panel.RenderingHints.VALUE_RENDER_QUALITY);
        p2.setRenderingHint(P5Panel.RenderingHints.KEY_ANTIALIASING, P5Panel.RenderingHints.VALUE_ANTIALIAS_ON);
        // draw background disc
        drawBackground(p2, width);
        // draw main part in all 4 directions in same color
        // x, y arrays used for drawing polygons
        // starting point is always (centerX, centerY)
        p2.setColor(MAIN_COLOR);
        final int[] x = new int[3];
        final int[] y = new int[3];
        x[0] = centerX;
        y[0] = centerY;
        drawMainNorth(p2, mainLength, mainWidth, x, y);
        drawMainEast(p2, mainLength, mainWidth, x, y);
        drawMainSouth(p2, mainLength, mainWidth, x, y);
        drawMainWest(p2, mainLength, mainWidth, x, y);
        
        drawBorderCircle(p2, width);
        
        drawDirectionMarker(p2, width);
    }


	private void drawBackground(final MazePanel p2, int width) {
		p2.setColor(16777215); // white
		final int x = centerX - size;
		final int y = centerY - size;
		final int w = 2 * size;// - 2 * CIRCLE_BORDER;
        p2.addFilledOval(x,y,w,w);
	}


	private void drawMainWest(MazePanel p2, int length, int width, int[] x, int[] y) {
		x[1] = centerX - length;
        y[1] = centerY;
        x[2] = centerX - width;
        y[2] = centerY + width;
        p2.addFilledPolygon(x, y, 3);

        y[2] = centerY - width;
        p2.addPolygon(x, y, 3);
	}
	private void drawMainEast(MazePanel p2, int length, int width, int[] x, int[] y) {
		// observation: the 2 triangles to the right are drawn the same
		// way as for the left if one inverts the sign for length and width
		// i.e. exchanges addition and subtraction
		drawMainWest(p2, -length, -width, x, y);
	}
	private void drawMainSouth(MazePanel p2, int length, int width, int[] x, int[] y) {
		x[1] = centerX;
        y[1] = centerY + length;
        x[2] = centerX + width;
        y[2] = centerY + width;
        p2.addFilledPolygon(x, y, 3);
        
        x[2] = centerX - width;
        p2.addPolygon(x, y, 3);
	}
	private void drawMainNorth(MazePanel p2, int length, int width, int[] x, int[] y) {
		// observation: the 2 triangles to the top are drawn the same
		// way as for the bottom if one inverts the sign for length and width
		// i.e. exchanges addition and subtraction
		drawMainSouth(p2, -length, -width, x, y);
	}

	private void drawBorderCircle(MazePanel p2, int width) {
		final int x = centerX - width / 2 + CIRCLE_BORDER;
		final int y = centerY - width / 2 + CIRCLE_BORDER;
		final int w = width - 2 * CIRCLE_BORDER;
		p2.setColor(MazePanel.CIRCLE_SHADE);
        p2.addArc(x, y, w, w, 45, 180);
        p2.setColor(MazePanel.CIRCLE_HIGHLIGHT);
        p2.addArc(x, y, w, w, 180 + 45, 180);
	}


	private void drawDirectionMarker(MazePanel p2, int width) {
		if (!Double.isNaN(markerRadius) && p2.getFont() != null) {
            
            int pos = (int) (width * markerRadius / 2);
            
            p2.setColor(MazePanel.MARKER_COLOR);
            /* original code
            drawMarker(g2, mid, mid - pos, trans.get("lbl.north"));
            drawMarker(g2, mid + pos, mid, trans.get("lbl.east"));
            drawMarker(g2, mid, mid + pos, trans.get("lbl.south"));
            drawMarker(g2, mid - pos, mid, trans.get("lbl.west"));
            */
            /* version with color highlighting but stable orientation 
             * Highlighting with MarkerColor which is white
             * use gold as color for others */
            // WARNING: north south confusion
            // currendDir South is going upward on the map
            if (CardinalDirection.South == currentDir)
            	p2.setColor(MazePanel.MARKER_COLOR);
            else
            	p2.setColor(MazePanel.goldWM);
            drawMarker(p2, centerX, centerY - pos, "N");
            if (CardinalDirection.East == currentDir)
            	p2.setColor(MazePanel.MARKER_COLOR);
            else
            	p2.setColor(MazePanel.goldWM);
            drawMarker(p2, centerX + pos, centerY, "E");
            // WARNING: north south confusion
            // currendDir North is going downwards on the map
            if (CardinalDirection.North == currentDir)
            	p2.setColor(MazePanel.MARKER_COLOR);
            else
            	p2.setColor(MazePanel.goldWM);
            drawMarker(p2, centerX, centerY + pos, "S");
            if (CardinalDirection.West == currentDir)
            	p2.setColor(MazePanel.MARKER_COLOR);
            else
            	p2.setColor(MazePanel.goldWM);
            drawMarker(p2, centerX - pos, centerY, "W");
        }
	}
 
    private void drawMarker(MazePanel p2, float x, float y, String str) {
    	p2.addMarker(x, y, str);
        
    }
 
    public double getScaler() {
        return scaler;
    }
    
    public void setScaler(double scaler) {
        this.scaler = scaler;
        repaint();
    }
    
    public double getMarkerRadius() {
        return markerRadius;
    }
    
    public void setMarkerRadius(double markerRadius) {
        this.markerRadius = markerRadius;
        repaint();
    }
    
    
}
