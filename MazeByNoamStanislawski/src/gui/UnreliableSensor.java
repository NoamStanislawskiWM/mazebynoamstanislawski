package gui;

import java.util.Timer;
import java.util.TimerTask;

import generation.CardinalDirection;
import generation.Maze;
import generation.MazeContainer;
import gui.Robot.Direction;

class UnreliableSensor extends ReliableSensor implements Runnable {
	
	private MazeContainer currentMaze;
	private UnreliableRobot robot;
	private Direction sensorDirection;
	private int energyForSensing = 1;
	private int distance = 0;
	private CardinalDirection direction;
	private float[] ps;
	int timeCounter = 0;
	
	public UnreliableSensor() {
		super();
	}

	@Override
	public int distanceToObstacle(int[] currentPosition, CardinalDirection currentDirection, float[] powersupply)
			throws Exception {
		
		robot.getCurrentPosition();
		CardinalDirection robotDirection = robot.getCurrentDirection(); // this is forward
		
		if(currentDirection.compareTo(robotDirection) == 0) {
			distance = robot.distanceToObstacle(Direction.FORWARD);
		}
		else if(currentDirection.compareTo(robotDirection.oppositeDirection()) == 0) {
			distance = robot.distanceToObstacle(Direction.BACKWARD);
		}
		else if(currentDirection.compareTo(robotDirection.rotateClockwise()) == 0) {
			distance = robot.distanceToObstacle(Direction.RIGHT);
		}
		else {
			distance = robot.distanceToObstacle(Direction.LEFT);
		}
		
		return distance;
	}

	@Override
	public void setMaze(Maze maze) {
		maze = currentMaze;

	}

	@Override
	public void setSensorDirection(Direction mountedDirection) {
		sensorDirection = mountedDirection;

	}
	
	public void setRobot(UnreliableRobot r) {
		robot = r;
	}

	@Override
	public float getEnergyConsumptionForSensing() {
		return energyForSensing;
	}

	@Override
	public void startFailureAndRepairProcess(int meanTimeBetweenFailures, int meanTimeToRepair)
			throws UnsupportedOperationException {

	}

	@Override
	public void stopFailureAndRepairProcess() throws UnsupportedOperationException {

	}
	
	private void failureAndRepair() throws InterruptedException {
		System.out.println("SLEEPING THREAD");
		Thread.sleep(robot.repairTime);
		
	}
	@Override
	public void run() {		
		Timer timer = new Timer("DistanceTimer");
		TimerTask timerTask = new TimerTask() {
			 public void run() {
						timeCounter += 1;
						//System.out.println(distance);
						if(robot.isAtExit()) {
							timer.cancel();
						}
						if(timeCounter == 20) {
							try {
								failureAndRepair();
								timeCounter = 0;
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
 

			}
		};


        timer.scheduleAtFixedRate(timerTask,0,200);//this line starts the timer at the same time its executed
	}

}
